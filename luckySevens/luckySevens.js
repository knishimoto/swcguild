function playDice() {
	
	var startMoney = document.getElementById("startMoney").value;
	var currentMoney = startMoney; 
	var rollNumber = 0;
	var maxMoney = startMoney;
	var highestRoll = 0; 

if (isNaN(startMoney) || startMoney < 1) {
			alert("Invalid amount, Please enter a valid number!");
		}
else {
	 	do {
			var dice1;
			var dice2;
			var total;
							
			rollNumber++;
							
			dice1 = Math.floor((Math.random() * 6) + 1);
			dice2 = Math.floor((Math.random() * 6 )+ 1);
			total = dice1 + dice2;
						
			if (total == 7) {
					currentMoney += 4;
					
					if (currentMoney > maxMoney) {
							maxMoney = currentMoney;
							highestRoll = rollNumber;
						}
				}
			else {
					currentMoney -= 1;
				}
			}
					
		while(currentMoney > 0);
					
		displayResult();
			document.getElementById("startingMoney").innerHTML = "$" + startMoney + ".00";
			document.getElementById("totalRolls").innerHTML = rollNumber;
			document.getElementById("highestAmount").innerHTML = "$" + maxMoney + ".00";
			document.getElementById("highestRollCount").innerHTML = highestRoll;
			
			document.getElementById("play").value = "Play Again";
			document.getElementById("startMoney").value = "";	
		}
}

function displayResult() {
	document.getElementById("result").style.display = "";

}
